function Header(){
    return(
        <header>
      <>
  <section className="headertop">
    <span id="name">Global News</span>
    <span id="hname">News</span>
    <form>
      <div className="search">
        <button>
          <span id="fonts" className="material-symbols-outlined">
            search
          </span>
        </button>
        <input
          className="search-input"
          type="text"
          placeholder="search for topics,locations & sources"
        />
        <button>
          <span id="fonts" className="material-symbols-outlined">
            arrow_drop_down
          </span>
        </button>
      </div>
    </form>
    <div className="hright">
      <span id="fontss" className="material-symbols-outlined">
        search
      </span>
      <span id="fontsss" className="material-symbols-outlined">
        apps
      </span>
      <span id="fontsss" className="material-symbols-outlined">
        account_circle
      </span>
    </div>
  </section>
  <section className="headerbottom">
    <a id="links" href="#">
      Home
    </a>
    <a id="links" href="#">
      For you
    </a>
    <a id="links" href="#">
      Following
    </a>
    <a id="links" href="#">
      News Showcase
    </a>
    <a id="links" href="#">
      India
    </a>
    <a id="links" href="#">
      World
    </a>
    <a id="links1" href="#">
      Local
    </a>
    <a id="links1" href="#">
      Business
    </a>
    <a id="links2" href="#">
      Technology
    </a>
    <a id="links2" href="#">
      Entertainment
    </a>
    <a id="links2" href="#">
      Sports
    </a>
    <a id="links2" href="#">
      Science
    </a>
    <a id="links2" href="#">
      Health
    </a>
  </section>
</>

      </header>
    )
}

export default Header